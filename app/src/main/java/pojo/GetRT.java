package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yanzb on 11/11/2018.
 */

public class GetRT {
    @SerializedName("NO_KEC")
    @Expose
    private String NO_KEC;

    @SerializedName("NO_KEL")
    @Expose
    private String NO_KEL;

    @SerializedName("NO_RW")
    @Expose
    private String NO_RW;

    @SerializedName("NO_RT")
    @Expose
    private String NO_RT;

    /**
     *
     * @return
     *
     */
    public String getNO_KEC() {
        return NO_KEC;
    }

    /**
     *
     * @param NO_KEC
     *
     */
    public void setNO_KEC(String NO_KEC) {
        this.NO_KEC = NO_KEC;
    }

    /**
     *
     * @return
     *
     */
    public String getNO_KEL() {
        return NO_KEL;
    }

    /**
     *
     * @param NO_KEL
     *
     */
    public void setNO_KEL(String NO_KEL) {
        this.NO_KEL = NO_KEL;
    }

    /**
     *
     * @return
     *
     */
    public String getNO_RW() {
        return NO_RW;
    }

    /**
     *
     * @param NO_RW
     *
     */
    public void setNO_RW(String NO_RW) {
        this.NO_RW = NO_RW;
    }

    /**
     *
     * @return
     *
     */
    public String getNO_RT() {
        return NO_RT;
    }

    /**
     *
     * @param NO_RT
     *
     */
    public void setNO_RT(String NO_RT) {
        this.NO_RT = NO_RT;
    }
}


