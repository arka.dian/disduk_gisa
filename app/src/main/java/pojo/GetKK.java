package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yanzb on 11/12/2018.
 */

public class GetKK {
    @SerializedName("NO_KK")
    @Expose
    private String NO_KK;

    @SerializedName("NAMA_KEP")
    @Expose
    private String NAMA_KEP;

    @SerializedName("ALAMAT")
    @Expose
    private String ALAMAT;

    @SerializedName("JML_KELUARGA")
    @Expose
    private String JML_KELUARGA;

    /**
     *
     * @return
     *
     */
    public String getNO_KK() {
        return NO_KK;
    }

    /**
     *
     * @param NO_KK
     *
     */
    public void setNO_KK(String NO_KK) {
        this.NO_KK = NO_KK;
    }

    /**
     *
     * @return
     *
     */
    public String getNAMA_KEP() {
        return NAMA_KEP;
    }

    /**
     *
     * @param NAMA_KEP
     *
     */
    public void setNAMA_KEP(String NAMA_KEP) {
        this.NAMA_KEP = NAMA_KEP;
    }
    /**
     *
     * @return
     *
     */
    public String getALAMAT() {
        return ALAMAT;
    }

    /**
     *
     * @param ALAMAT
     *
     */
    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    /**
     *
     * @return
     *
     */
    public String getJML_KELUARGA() {
        return JML_KELUARGA;
    }

    /**
     *
     * @param JML_KELUARGA
     *
     */
    public void setJML_KELUARGA(String JML_KELUARGA) {
        this.JML_KELUARGA = JML_KELUARGA;
    }

}



