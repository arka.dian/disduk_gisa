package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 28/11/17.
 */

public class GetNotifikasi {

    @SerializedName("notice_id")
    @Expose
    private String notice_id;

    @SerializedName("source_id")
    @Expose
    private String source_id;

    @SerializedName("notice_type")
    @Expose
    private String notice_type;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("notice_from")
    @Expose
    private String notice_from;

    @SerializedName("notice_to")
    @Expose
    private String notice_to;

    @SerializedName("notice_date")
    @Expose
    private String notice_date;

    @SerializedName("employee_name")
    @Expose
    private String employee_name;

    @SerializedName("photo")
    @Expose
    private String photo;


    /**
     *
     * @return
     *
     */
    public String getNotice_id() {
        return notice_id;
    }

    /**
     *
     * @param notice_id
     *
     */
    public void setNotice_id(String notice_id) {
        this.notice_id = notice_id;
    }

    /**
     *
     * @return
     *
     */
    public String getSource_id() {
        return source_id;
    }

    /**
     *
     * @param source_id
     *
     */
    public void setSource_id(String source_id) {
        this.source_id = source_id;
    }

    /**
     *
     * @return
     *
     */
    public String getNotice_type() {
        return notice_type;
    }

    /**
     *
     * @param notice_type
     *
     */
    public void setNotice_type(String notice_type) {
        this.notice_type = notice_type;
    }

    /**
     *
     * @return
     *
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     *
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *
     */
    public String getNotice_from() {
        return notice_from;
    }

    /**
     *
     * @param notice_from
     *
     */
    public void setNotice_from(String notice_from) {
        this.notice_from = notice_from;
    }

    /**
     *
     * @return
     *
     */
    public String getNotice_to() {
        return notice_to;
    }

    /**
     *
     * @param notice_to
     *
     */
    public void setNotice_to(String notice_to) {
        this.notice_to = notice_to;
    }

    /**
     *
     * @return
     *
     */
    public String getNotice_date() {
        return notice_date;
    }

    /**
     *
     * @param notice_date
     *
     */
    public void setNotice_date(String notice_date) {
        this.notice_date = notice_date;
    }

    /**
     *
     * @return
     *
     */
    public String getEmployee_name() {
        return employee_name;
    }

    /**
     *
     * @param employee_name
     *
     */
    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    /**
     *
     * @return
     *
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     *
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
