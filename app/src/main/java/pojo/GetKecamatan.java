package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yanzb on 11/11/2018.
 */

public class GetKecamatan {
    @SerializedName("NO_KEC")
    @Expose
    private String NO_KEC;

    @SerializedName("NAMA_KEC")
    @Expose
    private String NAMA_KEC;
    /**
     *
     * @return
     *
     */
    public String getNO_KEC() {
        return NO_KEC;
    }

    /**
     *
     * @param NO_KEC
     *
     */
    public void setNO_KEC(String NO_KEC) {
        this.NO_KEC = NO_KEC;
    }

    /**
     *
     * @return
     *
     */
    public String getNAMA_KEC() {
        return NAMA_KEC;
    }

    /**
     *
     * @param NAMA_KEC
     *
     */
    public void setNAMA_KEC(String NAMA_KEC) {
        this.NAMA_KEC = NAMA_KEC;
    }

    /**
     *
     * @return
     *
     */
}
