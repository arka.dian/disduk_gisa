package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yanzb on 11/12/2018.
 */

public class GetBIO {
    @SerializedName("NO_KK")
    @Expose
    private String NO_KK;

    @SerializedName("NIK")
    @Expose
    private String NIK;

    @SerializedName("NAMA_LGKP")
    @Expose
    private String NAMA_LGKP;

    @SerializedName("JENIS_KLMIN")
    @Expose
    private String JENIS_KLMIN;

    @SerializedName("TMPT_LHR")
    @Expose
    private String TMPT_LHR;

    @SerializedName("TGL_LHR")
    @Expose
    private String TGL_LHR;

    @SerializedName("UMUR")
    @Expose
    private String UMUR;

    @SerializedName("STAT_HBKEL")
    @Expose
    private String STAT_HBKEL;

    @SerializedName("AGAMA")
    @Expose
    private String AGAMA;

    @SerializedName("STATUS_KAWIN")
    @Expose
    private String STATUS_KAWIN;

    @SerializedName("GOL_DRH")
    @Expose
    private String GOL_DRH;

    @SerializedName("PEKERJAAN")
    @Expose
    private String PEKERJAAN;

    @SerializedName("STATUS_KK")
    @Expose
    private String STATUS_KK;

    @SerializedName("STATUS_KIA")
    @Expose
    private String STATUS_KIA;

    @SerializedName("STATUS_KTP")
    @Expose
    private String STATUS_KTP;

    @SerializedName("STATUS_REKAM")
    @Expose
    private String STATUS_REKAM;

    @SerializedName("STATUS_AKTA_LHR")
    @Expose
    private String STATUS_AKTA_LHR;

    @SerializedName("NO_AKTA_LHR")
    @Expose
    private String NO_AKTA_LHR;

    @SerializedName("STATUS_AKTA_KWN")
    @Expose
    private String STATUS_AKTA_KWN;

    @SerializedName("NO_AKTA_KWN")
    @Expose
    private String NO_AKTA_KWN;

    @SerializedName("STATUS_AKTA_CRAI")
    @Expose
    private String STATUS_AKTA_CRAI;

    @SerializedName("NO_AKTA_CRAI")
    @Expose
    private String NO_AKTA_CRAI;

    /**
     *
     * @return
     *
     */
    public String getNO_KK() {
        return NO_KK;
    }

    /**
     *
     * @param NO_KK
     *
     */
    public void setNO_KK(String NO_KK) {
        this.NO_KK = NO_KK;
    }

    /**
     *
     * @return
     *
     */
    public String getNIK() {
        return NIK;
    }

    /**
     *
     * @param NIK
     *
     */
    public void setNIK(String NIK) {
        this.NIK = NIK;
    }
    /**
     *
     * @return
     *
     */
    public String getNAMA_LGKP() {
        return NAMA_LGKP;
    }

    /**
     *
     * @param NAMA_LGKP
     *
     */
    public void setNAMA_LGKP(String NAMA_LGKP) {
        this.NAMA_LGKP = NAMA_LGKP;
    }

    /**
     *
     * @return
     *
     */
    public String getJENIS_KLMIN() {
        return JENIS_KLMIN;
    }

    /**
     *
     * @param JENIS_KLMIN
     *
     */
    public void setJENIS_KLMIN(String JENIS_KLMIN) {
        this.JENIS_KLMIN = JENIS_KLMIN;
    }
    /**
     *
     * @return
     *
     */
    public String getTMPT_LHR() {
        return TMPT_LHR;
    }

    /**
     *
     * @param TMPT_LHR
     *
     */
    public void setTMPT_LHR(String TMPT_LHR) {
        this.TMPT_LHR = TMPT_LHR;
    }
    /**
     *
     * @return
     *
     */
    public String getTGL_LHR() {
        return TGL_LHR;
    }

    /**
     *
     * @param TGL_LHR
     *
     */
    public void setTGL_LHR(String TGL_LHR) {
        this.TGL_LHR = TGL_LHR;
    }
    /**
     *
     * @return
     *
     */
    public String getUMUR() {
        return UMUR;
    }

    /**
     *
     * @param UMUR
     *
     */
    public void setUMUR(String UMUR) {
        this.UMUR = UMUR;
    }
    /**
     *
     * @return
     *
     */
    public String getSTAT_HBKEL() {
        return STAT_HBKEL;
    }

    /**
     *
     * @param STAT_HBKEL
     *
     */
    public void setSTAT_HBKEL(String STAT_HBKEL) {
        this.STAT_HBKEL = STAT_HBKEL;
    }
    /**
     *
     * @return
     *
     */
    public String getAGAMA() {
        return AGAMA;
    }

    /**
     *
     * @param AGAMA
     *
     */
    public void setAGAMA(String AGAMA) {
        this.AGAMA = AGAMA;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_KAWIN() {
        return STATUS_KAWIN;
    }

    /**
     *
     * @param STATUS_KAWIN
     *
     */
    public void setSTATUS_KAWIN(String STATUS_KAWIN) {
        this.STATUS_KAWIN = STATUS_KAWIN;
    }
    /**
     *
     * @return
     *
     */
    public String getGOL_DRH() {
        return GOL_DRH;
    }

    /**
     *
     * @param GOL_DRH
     *
     */
    public void setGOL_DRH(String GOL_DRH) {
        this.GOL_DRH = GOL_DRH;
    }
    /**
     *
     * @return
     *
     */
    public String getPEKERJAAN() {
        return PEKERJAAN;
    }

    /**
     *
     * @param PEKERJAAN
     *
     */
    public void setPEKERJAAN(String PEKERJAAN) {
        this.PEKERJAAN = PEKERJAAN;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_KK() {
        return STATUS_KK;
    }

    /**
     *
     * @param STATUS_KK
     *
     */
    public void setSTATUS_KK(String STATUS_KK) {
        this.STATUS_KK = STATUS_KK;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_KIA() {
        return STATUS_KIA;
    }

    /**
     *
     * @param STATUS_KIA
     *
     */
    public void setSTATUS_KIA(String STATUS_KIA) {
        this.STATUS_KIA = STATUS_KIA;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_KTP() {
        return STATUS_KTP;
    }

    /**
     *
     * @param STATUS_KTP
     *
     */
    public void setSTATUS_KTP(String STATUS_KTP) {
        this.STATUS_KTP = STATUS_KTP;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_REKAM() {
        return STATUS_REKAM;
    }

    /**
     *
     * @param STATUS_REKAM
     *
     */
    public void setSTATUS_REKAM(String STATUS_REKAM) {
        this.STATUS_REKAM = STATUS_REKAM;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_AKTA_LHR() {
        return STATUS_AKTA_LHR;
    }

    /**
     *
     * @param STATUS_AKTA_LHR
     *
     */
    public void setSTATUS_AKTA_LHR(String STATUS_AKTA_LHR) {
        this.STATUS_AKTA_LHR = STATUS_AKTA_LHR;
    }
    /**
     *
     * @return
     *
     */
    public String getNO_AKTA_LHR() {
        return NO_AKTA_LHR;
    }

    /**
     *
     * @param NO_AKTA_LHR
     *
     */
    public void setNO_AKTA_LHR(String NO_AKTA_LHR) {
        this.NO_AKTA_LHR = NO_AKTA_LHR;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_AKTA_KWN() {
        return STATUS_AKTA_KWN;
    }

    /**
     *
     * @param STATUS_AKTA_KWN
     *
     */
    public void setSTATUS_AKTA_KWN(String STATUS_AKTA_KWN) {
        this.STATUS_AKTA_KWN = STATUS_AKTA_KWN;
    }
    /**
     *
     * @return
     *
     */
    public String getNO_AKTA_KWN() {
        return NO_AKTA_KWN;
    }

    /**
     *
     * @param NO_AKTA_KWN
     *
     */
    public void setNO_AKTA_KWN(String NO_AKTA_KWN) {
        this.NO_AKTA_KWN = NO_AKTA_KWN;
    }
    /**
     *
     * @return
     *
     */
    public String getSTATUS_AKTA_CRAI() {
        return STATUS_AKTA_CRAI;
    }

    /**
     *
     * @param STATUS_AKTA_CRAI
     *
     */
    public void setSTATUS_AKTA_CRAI(String STATUS_AKTA_CRAI) {
        this.STATUS_AKTA_CRAI = STATUS_AKTA_CRAI;
    }
    /**
     *
     * @return
     *
     */
    public String getNO_AKTA_CRAI() {
        return NO_AKTA_CRAI;
    }

    /**
     *
     * @param NO_AKTA_CRAI
     *
     */
    public void setNO_AKTA_CRAI(String NO_AKTA_CRAI) {
        this.NO_AKTA_CRAI = NO_AKTA_CRAI;
    }
}




