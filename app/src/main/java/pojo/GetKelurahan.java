package pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yanzb on 11/11/2018.
 */

public class GetKelurahan {
    @SerializedName("NO_KEC")
    @Expose
    private String NO_KEC;

    @SerializedName("NO_KEL")
    @Expose
    private String NO_KEL;

    @SerializedName("NAMA_KEL")
    @Expose
    private String NAMA_KEL;
    /**
     *
     * @return
     *
     */
    public String getNO_KEC() {
        return NO_KEC;
    }

    /**
     *
     * @param NO_KEC
     *
     */
    public void setNO_KEC(String NO_KEC) {
        this.NO_KEC = NO_KEC;
    }

    /**
     *
     * @return
     *
     */
    public String getNO_KEL() {
        return NO_KEL;
    }

    /**
     *
     * @param NO_KEL
     *
     */
    public void setNO_KEL(String NO_KEL) {
        this.NO_KEL = NO_KEL;
    }

    /**
     *
     * @return
     *
     */
    public String getNAMA_KEL() {
        return NAMA_KEL;
    }

    /**
     *
     * @param NAMA_KEL
     *
     */
    public void setNAMA_KEL(String NAMA_KEL) {
        this.NAMA_KEL = NAMA_KEL;
    }

    /**
     *
     * @return
     *
     */
}

