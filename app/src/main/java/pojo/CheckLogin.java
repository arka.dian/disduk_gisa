
package pojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckLogin {

    @SerializedName("msgType")
    @Expose
    private String msgType;
    @SerializedName("msgText")
    @Expose
    private String msgText;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("user_name")
    @Expose
    private String user_name;

    /**
     *
     * @return
     * The msgType
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     *
     * @param msgType
     * The msgType
     */
    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    /**
     *
     * @return
     * The msgText
     */
    public String getMsgText() {
        return msgText;
    }

    /**
     *
     * @param msgText
     * The msgText
     */
    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    /**
     *
     * @return
     * The UserId
     */
    public String getUserId() {
        return user_id;
    }

    /**
     *
     * @param user_id
     * The User_id
     */
    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    /**
     *
     * @return
     * The UserName
     */
    public String getUserName() {
        return user_name;
    }

    /**
     *
     * @param user_name
     * The User_name
     */
    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

}